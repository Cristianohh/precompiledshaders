#include <assert.h>
#include <stdlib.h>
#include <string>
#include "system.h"
#include "texture.h"
#include "stb_image.h"
#include <android/log.h>
#include <android/asset_manager.h>
#include <stdio.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
// Loads a PNG texture and returns a handle
//
// PNG loading code provided as public domain by Sean Barrett (http://nothings.org/)
GLuint load_texture(const char* filename)
{
    char*   file_data = NULL;
    size_t  file_size = 0;
    uint8_t*    texture_data = NULL;
    int width, height, components;
    GLuint      texture;
    GLenum      format;
    int         result;

    read_file( filename, &file_data, &file_size );

    texture_data = stbi_load_from_memory( (unsigned char*)file_data, (int)file_size, &width, &height, &components, 0);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    switch( components ) {
        case 1: {
            // Gray
            format = GL_LUMINANCE;
            break;
        }
        case 2: {
            // Gray and Alpha
            format = GL_LUMINANCE_ALPHA;
            break;
        }
        case 3: {
            // RGB
            format = GL_RGB;
            break;
        }
        case 4: {
            // RGBA
            format = GL_RGBA;
            break;
        }
        default: {
            // Unknown format
            assert(0);
            return 0;
        }
    }

    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, texture_data);
    //glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(texture_data);

    return texture;
}
