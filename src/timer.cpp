/*! @file timer.c
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include "timer.h"
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <time.h>

static struct timespec _time_difference(struct timespec a, struct timespec b)
{
    struct timespec temp;
    temp.tv_sec = a.tv_sec-b.tv_sec;
    temp.tv_nsec = a.tv_nsec-b.tv_nsec;
    return temp;
}

struct Timer
{
    struct timespec start_time;
    struct timespec prev_time;
};

Timer* create_timer(void)
{
    Timer* timer = (Timer*)calloc(1, sizeof(*timer));
    reset_timer(timer);
    return timer;
}
void destroy_timer(Timer* timer)
{
    free(timer);
}
void reset_timer(Timer* timer)
{
    struct timespec time;
    clock_gettime(CLOCK_MONOTONIC, &time);
    timer->prev_time = timer->start_time = time;
}
double get_delta_time(Timer* timer)
{
    struct timespec time;
    struct timespec diff;
    clock_gettime(CLOCK_MONOTONIC, &time);
    diff = _time_difference(time, timer->prev_time);
    timer->prev_time = time;
    return diff.tv_sec + diff.tv_nsec*1.0/1000000000;
}
double get_running_time(Timer* timer)
{
    struct timespec time;
    struct timespec diff;
    clock_gettime(CLOCK_MONOTONIC, &time);
    diff = _time_difference(time, timer->start_time);
    return diff.tv_sec + diff.tv_nsec*1.0/1000000000;
}