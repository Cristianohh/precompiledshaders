#ifndef __DRR_SHADER_H__
#define __DRR_SHADER_H__

#include "system.h"

GLuint compile_shader( GLenum type, const char* shader_data, int shader_size );
GLuint link_shader( GLuint vertex_shader_handle, GLuint fragment_shader_handle );
GLuint create_ui_program(const char* vertex_shader_filename,
                       const char* fragment_shader_filename);
                       
// Loads, compiles, and links the shader for the given file and returns the program id
GLuint load_shader( const char* shader_file_name );

// Precompiled shader methods
void precompile_shader( const char* shader_file_name, const char* binary_filename, const char* binary_format_filename );
GLuint loadProgramBinary( const char* binary_filename, GLenum binaryFormat);

#endif // __DRR_SHADER_H__