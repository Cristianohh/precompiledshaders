#ifndef __DRR_MODEL_H__
#define __DRR_MODEL_H__

#include <vector>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "fonts.h"

typedef struct _vertex_t
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec4 color;
} vertex_t;

typedef struct _mesh_data_t
{
    int vertex_size;
    int vertex_count;
    int index_size;
    int index_count;
} mesh_data_t;

// Model data structure
typedef struct _model_t
{
    // interleaved vertex data
    GLuint vertex_buffer;
    
    // index buffer object
    GLuint index_buffer;
    
    // mesh data
	vertex_t* vertices;
    unsigned int* indices;
    
    unsigned int  num_vertices;
    unsigned int  num_indices;

    // shader attribute handles
    GLuint shader_program;
    GLint  position_attrib;
    GLint  normal_attrib;
    GLint  color_attrib;
    
    int shader_prog;

    _model_t(): shader_prog(0){}

} model_t;

model_t* create_cube_model( const char* shader );
model_t* create_cube_model_precompiled( const char* binary_filename, const char* binary_format_filename );
static model_t* complete_cube( model_t* _cube_model );
void draw_cube( model_t* model, glm::mat4* view_projection_matrix, glm::mat4* world_matrix, GLenum type );

ui* create_interface( const char* shader, unsigned int width, unsigned int height );
void draw_interface( ui* user_interface );


void init_shader( model_t* model, const char* shader );
void init_precompiled_shader( model_t* model, const char* binary_filename, const char* binary_format_filename );
static void bind_attributes( model_t* model );

#endif // __DRR_MODEL_H__
