#include <assert.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctime>

// Android includes
#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include "system.h"

const char* GLErrMsg[] = {
        "GL_INVALID_ENUM",
        "GL_INVALID_VALUE",
        "GL_INVALID_OPERATION",
        "GL_OUT_OF_MEMORY",
        "unknown",
};

char cCurrentPath[FILENAME_MAX];

AAssetManager* g_pManager = NULL;

void SetAssetManager( AAssetManager* pManager )
{
    g_pManager = pManager;
}

// Read the contents of the give file return the content and the file size.
// The calling function is responsible for free the memory allocated for Content.
void read_file( const char* FileName, char** Content, unsigned int* Size )
{  
    assert( g_pManager );
    
    // Open file
    AAsset* pFile = AAssetManager_open( g_pManager, FileName, AASSET_MODE_UNKNOWN );

    if( pFile != NULL )
    {
        // Determine file size
        off_t FileSize = AAsset_getLength( pFile );
        
        // Read data
        char* pData = (char*)malloc( FileSize );
        AAsset_read( pFile, pData, FileSize );
    
        // Allocate space for the file content
        *Content = (char*)malloc( FileSize );
    
        // Copy the content
        memcpy( *Content, pData, FileSize );
        *Size = FileSize;
        
        // Close the file
        AAsset_close( pFile );

        free( pData );
    }
}

float rand_f ( )
{
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}
