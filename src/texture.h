#ifndef __TEXTURE_LDR_H__
#define __TEXTURE_LDR_H__

#include "opengl.h"

GLuint load_texture(const char* filename);

#endif // header guard
