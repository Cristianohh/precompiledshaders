#ifndef __DRR_APP_H__
#define __DRR_APP_H__

void app_init( unsigned int width, unsigned int height );
void app_update( float fElapsedTime );
void app_render( void );


#endif // __DRR_APP_H__
