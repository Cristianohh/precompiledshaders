#ifndef __DRR_SYSTEM_H__
#define __DRR_SYSTEM_H__

#include <android/log.h>
 
extern const char* GLErrMsg[];

#define LOG_TAG     "PrecompiledApp"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)

#define checkOGLError() {\
    GLuint E;\
    if ( GL_NO_ERROR != (E = glGetError()) )\
        { LOGE("OpenGL Error: error 0x%x = %s", E, GLErrMsg[E-GL_INVALID_ENUM]); }\
    }

// Read the contents of the give file return the content and the file size
void read_file( const char* FileName, char** Content, unsigned int* Size );
float rand_f( );

#endif // __DRR_SYSTEM_H__
