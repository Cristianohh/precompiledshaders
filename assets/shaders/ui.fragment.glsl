precision highp float;

uniform sampler2D s_texture;
uniform vec4 u_color;

varying vec2 v_tex_coord;

void main()
{
    gl_FragColor = u_color * texture2D( s_texture, vec2(v_tex_coord.x, v_tex_coord.y) );
}
