#ifdef FRAGMENT
precision mediump float;
#endif


varying vec4 v_Col;
varying vec3 v_normal;


uniform mat4 u_view_proj_matrix;
uniform mat4 u_world_matrix;



#ifdef VERTEX

attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec4 a_color;

void main() 
{

    v_Col = a_color;

	// Transform normal to view space
    v_normal = ( u_world_matrix * vec4(a_normal, 0.0) ).xyz;

	// Convert position into screen space
	gl_Position = u_view_proj_matrix * u_world_matrix * vec4(a_position, 1.0);
	
}
#endif
 
 
#ifdef FRAGMENT

vec3 direction_to_light = vec3(0.0, 0.0, 1.0);
vec3 direction_to_eye = vec3(0.0, 0.0, 1.0);

void main()
{
	float diffuseFactor = dot(v_normal, direction_to_light);
	float specularFactor = pow(dot(reflect(direction_to_eye, v_normal), direction_to_light), 128.0);
	gl_FragColor = (diffuseFactor + specularFactor) * vec4(0.1, 1.0, 1.0, 1.0)
	  * v_Col;
}

#endif