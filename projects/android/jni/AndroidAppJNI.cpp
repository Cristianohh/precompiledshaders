#include <jni.h>
#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "App.h"
#include "opengl.h"

// Forward declare set asset manager.
void SetAssetManager( AAssetManager* pManager );

#ifdef __cplusplus
extern "C" 
{
#endif // __cplusplus

    // JNI function declarations
    JNIEXPORT void JNICALL Java_com_intel_precompiledshaders_PrecompiledLib_init(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT void JNICALL Java_com_intel_precompiledshaders_PrecompiledLib_step(JNIEnv * env, jobject obj, jfloat fElapsedTime);
    JNIEXPORT void JNICALL Java_com_intel_precompiledshaders_PrecompiledLib_createAssetManager(JNIEnv* env, jobject obj, jobject assetManager);
    
#ifdef __cplusplus
}
#endif // __cplusplus


// JNI function definitions

JNIEXPORT void JNICALL Java_com_intel_precompiledshaders_PrecompiledLib_init(JNIEnv * env, jobject obj,  jint width, jint height)
{
    app_init( width, height );
}

JNIEXPORT void JNICALL Java_com_intel_precompiledshaders_PrecompiledLib_step(JNIEnv * env, jobject obj, jfloat fElapsedTime)
{
    // Update the app
    app_update( fElapsedTime );
    
    // Render a frame from the app
    app_render();
}

JNIEXPORT void JNICALL Java_com_intel_precompiledshaders_PrecompiledLib_createAssetManager(JNIEnv* env, jobject obj, jobject assetManager)
{
    AAssetManager* mgr = AAssetManager_fromJava( env, assetManager );
    assert( mgr );
    
    // Store the asset manager for future use.
    SetAssetManager( mgr );
}




